from PyQt5.QtWidgets import QApplication, QMainWindow

import planer_form

class IpPreg(QMainWindow, planer_form.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.pushButton.clicked.connect(self.btn_click)
        self.listWidget = []
    def btn_click(self):
        opinion1 = self.calendarWidget.selectedDate().toString('dd.MM.yyyy')
        opinion2 = self.timeEdit.dateTime().toString('hh:mm:ss')
        opinion3 = self.lineEdit.text()

        self.lineEdit.clear()

        self.listWidget.append(f'{opinion1} {opinion2} - {opinion3}')
        self.listWidget.sort()


if __name__ == '__main__':
    app = QApplication([])
    wnd = IpPreg()
    wnd.show()
    app.exec()

